var express = require('express');
var bodyParser = require('body-parser');
var compression = require('compression');


//app express
var app = express();

//allow app can curl
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

//setDelay Exu
app.use(function(req,res,next){setTimeout(next,100)});

app.set('port', (process.env.PORT || 3001));
app.use('/asset', express.static('./app/public'));

//include file
var site = require('./express_module/site');
var posts = require('./express_module/posts');
var getData = require('./express_module/getData');
var postData = require('./express_module/postData');


//config public
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(compression());
app.use(express.static(__dirname + '/public'));
app.use(site.log);

//---------Rounter Zone----------// index web
app.get('/', site.index);

//post
app.post('/post/addNews', posts.addNews);

app.post('/post/addList',function(req, res, next){
    postData.addList(req, res, next);
});

//get Data
app.get('/get/quiz', function (req, res, next) {
    getData.getQuiz(req, res, next);
});

app.get('/get/quiz/:idQuiz', function (req, res, next) {
    getData.getQuiz(req, res, next);
});

//---------Rounter Zone----------// start server

app.listen(app.get('port'), () => {
    console.log(`Find the server at: http://localhost:${app.get('port')}/`); // eslint-disable-line no-console
});