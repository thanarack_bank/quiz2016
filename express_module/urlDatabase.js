var mongoose = require('mongoose');
var urlDatabase = "mongodb://localhost:27017/quiz";
var Schema = mongoose.Schema;

var database = function () {
    mongoose.connect(urlDatabase);
}

database.prototype.mongoose = mongoose;
database.prototype.Schema = Schema;

module.exports = new database();