//เรียกใช้ mongoose
var database = require('./urlDatabase');

var getData = function () {
    
};

//เลือก schema ที่ต้องการแสดงผล
getData.prototype.SchemaQuiz = new database.Schema({id: Number, name: String, img: Object});

getData.prototype.getQuiz = function (req, res, next) {

    //ส่ง schema ไป ชื่อ model,ข้อมูล schema,ชื่อคอเลคชั่น
    var Quiz = database.mongoose.model('sQuiz', this.SchemaQuiz, 'quiz');
    var idQuiz = req.params.idQuiz;
    var condition = null;

    //เมื่อทำการเรียกข้อมูล paremeter : ชื่อ function , ข้อมูลที่แนบมา
    var callStatus = function (callback, docs) {
        callback(docs);
    }

    //รอรับข้อมูลแล้วส่งไปหน้าจอ
    var successRes = function (docs) {
        res.statusCode = 200;
        res.send(docs);
        res.end();
    }

    var failRes = function () {
        res.statusCode = 404;
        res.send({status: 404});
        res.end();
    }

    if (idQuiz) {
        condition = {
            id: idQuiz
        };
    } else {
        condition = {};
    }

    //find = condition,field,option,callback
    Quiz
        .find(condition, {}, {}, function (err, docs) {
            if (docs) {
                callStatus(successRes, docs);
            } else {
                callStatus(failRes, null);
            }
        });

    //สิ้นสุดการทำงาน res.end(); next();
}

module.exports = new getData();