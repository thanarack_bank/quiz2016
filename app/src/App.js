import React, { Component } from 'react';

//Loading Router
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

//Component
import Layout from './module/Layout';
import Main from './module/Main';
import About from './module/About';
import Courses from './module/Courses';
import Play from './module/Play';
//import Hot from './module/Hot';
import NoMatch from './module/NoMatch';
//import Categored from './module/Categored';
import CategoredID from './module/CategoredID';
import Login from './module/Login';
import Singup from './module/Singup';


//console.log(HlistQuiz);
class App extends Component {
    render() {
        return (
            <Router history={browserHistory}>
                <Route path="/" component={Layout}>
                    <IndexRoute component={Main}></IndexRoute>
                    <Route path="courses" component={Courses}></Route>
                    <Route path="about" component={About}></Route>
                    <Route path="login" component={Login}></Route>
                    <Route path="singup" component={Singup} />
                    <Route path="play/:playID" component={Play}></Route>
                    <Route path="categored/:categoryID" component={CategoredID}></Route>
                    <Route path="*" component={NoMatch} />
                </Route>
            </Router>
        );
    }
}

export default App;
