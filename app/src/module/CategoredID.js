import React,{Component} from 'react';

class CategoredID extends Component{
    render(){
        const {params} = this.props;
        const {categoryID} = params;
        return(<h1>CategoredID Component ID : {categoryID}</h1>);
    }
}

export default CategoredID;