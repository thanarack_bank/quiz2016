import React, {Component} from 'react';
import {Card, Icon, Image} from 'semantic-ui-react';
import {Link} from 'react-router';
import NumberFormat from 'react-number-format';

class QuizCard extends Component {
    render() {
        const {data} = this.props;
        return (
            <Card>
                <Link to={'/play/' + data.id}><Image src={data.img.small}/>
                </Link>
                <Card.Content>
                    <Card.Header>
                        <Link to={'/play/' + data.id}>{data.title}</Link>
                    </Card.Header>
                    <Card.Meta>Join in 2016</Card.Meta>
                    <Card.Description>{data.desc}</Card.Description>
                </Card.Content>
                <Card.Content extra>
                    <Link className='left-icon' to='javascript:void(0)'><Icon name='group'/>
                        <NumberFormat
                            value={data.played}
                            displayType={'text'}
                            thousandSeparator={true}/></Link>
                    <Link className='right-icon' to={'/play/' + data.id}><Icon name='video play'/>
                        เล่นควิช</Link>
                </Card.Content>
            </Card>

        );
    }
}

export default QuizCard;