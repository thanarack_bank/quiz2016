import React, {Component} from 'react';
import QuizCard from './QuizCard';
import {Card} from 'semantic-ui-react';

class ListQuiz extends Component {

    render() {
        const {dataQuiz,perRow} = this.props;
        const listQuizNew = dataQuiz.map((data) => <QuizCard key={data._id} data={data}/>);

        return (
            <Card.Group itemsPerRow={perRow || 6}>
                {listQuizNew}
            </Card.Group>
        );

    }

}

export default ListQuiz;