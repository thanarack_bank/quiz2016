import React, {Component} from 'react';
import {List, Button, Image} from 'semantic-ui-react';

class ListPlayerTop extends Component {
    render() {
        const {playID, numShow} = this.props;
        var listPlayerTop = [];
        for (var i = 1; i <= numShow; i++) {
            var numberQuiz = (i <= 9
                ? (("0" + i).slice(-2))
                : i);
            listPlayerTop.push(
                <List.Item key={'idPlayer' + i}>
                    <List.Content floated='right'>
                        <Button>เพิ่มเพื่อน</Button>
                    </List.Content>
                    <Image avatar src='http://semantic-ui.com/images/avatar/small/daniel.jpg'/>
                    <List.Content>
                        <List.Header as='a'>Daniel Louise</List.Header>
                        <List.Description>คะแนนที่ทำได้ : 10</List.Description>
                    </List.Content>
                </List.Item>
            );
        }

        return (
            <div>
                <List divided relaxed>
                    {listPlayerTop}
                </List>
            </div>
        );
    }
}

export default ListPlayerTop;