import React, {Component} from 'react';
import {List} from 'semantic-ui-react';

class ListOnQuiz extends Component {
    render() {
        const {playID, numShow} = this.props;
        var listOnQuizComponent = [];
        for (var i = 1; i <= numShow; i++) {
            var numberQuiz = (i <= 9
                ? (("0" + i).slice(-2))
                : i);
            listOnQuizComponent.push(
                <List.Item key={'idQuiz' + i}>
                    <List.Icon name='video play outline' size='large' verticalAlign='middle'/>
                    <div className='runQuiz'>ข้อที่ {numberQuiz}</div>
                    <List.Content>
                        <List.Header>แบบทดสอบความรู้ทั่วไป
                        </List.Header>
                        <List.Description>มีเฉลย</List.Description>
                    </List.Content>
                </List.Item>
            );
        }
        return (
            <div>
                <List divided relaxed>
                    <List.Item>
                        <List.Content>
                            <List.Header>Section 1 : แบบทดสอบความรู้ทั่วไป
                            </List.Header>
                        </List.Content>
                    </List.Item>
                    {listOnQuizComponent}
                </List>
            </div>
        );
    }
}

export default ListOnQuiz;