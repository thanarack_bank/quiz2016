import React, {Component} from 'react';
import {List, Button, Icon, Comment} from 'semantic-ui-react';

class ListCommentQuiz extends Component {
    render() {
        const {playID, numShow} = this.props;
        var listComment = [];
        for (var i = 1; i <= numShow; i++) {
            var numberQuiz = (i <= 9
                ? (("0" + i).slice(-2))
                : i);
            listComment.push(
                <Comment key={'comment' + i}>
                    <Comment.Avatar
                        as='a'
                        src='http://semantic-ui.com/images/avatar/small/stevie.jpg'/>
                    <Comment.Content>
                        <Comment.Author>Stevie Feliciano</Comment.Author>
                        <Comment.Metadata>
                            <div>2 days ago</div>
                            <div>
                                <Icon name='star'/>
                                5 Faves
                            </div>
                        </Comment.Metadata>
                        <Comment.Text>
                            Hey guys, I hope this example comment is helping you read this documentation.
                        </Comment.Text>
                        <Comment.Actions>
                            <Comment.Action>ถูกใจ</Comment.Action>
                            <Comment.Action>ตอบกลับ</Comment.Action>
                        </Comment.Actions>
                    </Comment.Content>
                </Comment>
            );
        }

        return (
            <div>
                <Comment.Group>
                    {listComment}
                </Comment.Group>
                <div className='right'>
                    <Button label={1048} content='ความเห็นทั้งหมด' labelPosition='right'/>
                    <Button content='ดูความเห็นเพิ่ม' icon='right arrow' labelPosition='right'/>
                </div>
            </div>
        );
    }
}

export default ListCommentQuiz;