import React, {Component} from 'react';
import axios from 'axios';
import {
    Container,
    Comment,
    Grid,
    Header,
    Image,
    Button,
    Icon,
    Rating,
    Menu,
    Segment,
    List
} from 'semantic-ui-react';
//import {HlistQuiz} from '../http/HlistQuiz';
import {Link} from 'react-router';

//loadComponentPlay
import QuizDetail from './Play/QuizDetail';
import ListOnQuiz from './Play/ListOnQuiz';
import ListPlayerTop from './Play/ListPlayerTop';
import ListCommentQuiz from './Play/ListCommentQuiz';
import NumberFormat from 'react-number-format';
import OnLoader from './OnLoader';

class Play extends Component {

    constructor(props) {
        super(props);
        this.state = {
            playEnable: false,
            addToWL: false,
            activeItem: 'menu1',
            quizData: [],
            loaderStatus: false
        };
    }

    componentDidMount() {
        axios
            .get('http://localhost:3001/get/Quiz/' + this.props.params.playID)
            .then(res => {
                const quizData = res.data;
                this.setState((prevState) => ({
                    quizData: quizData[0],
                    loaderStatus: !prevState.loaderStatus
                }));
            });
    }

    componentWillUnmount() {
        this.setState({quizData: []});
    }

    playQuiz = () => {
        this.setState(prevState => ({
            playEnable: !prevState.playEnable
        }));
    }

    addToWL = () => {
        this.setState(prevState => ({
            addToWL: !prevState.addToWL
        }));
    }

    payConver(pay) {
        let text = null;
        if (pay === 'free') {
            text = 'ฟรี';
        }
        if (pay === 'pay') {
            text = 'ชำระเงิน';
        }
        return text;
    }

    langConvert(lang) {
        var textLang = '';
        var langs = '' + lang + '';
        var setLang = langs.split(',');
        setLang.map(res => {
            switch (res) {
                case 'th':
                    res = 'ไทย';
                    break;
                case 'en':
                    res = 'อังกฤษ';
                    break;
            }
            textLang += res + ',';
        })
        return (
            <span>{textLang}</span>
        );
    }

    calRate(rate) {
        let point = 0;
        if (rate >= 1000) {
            point = 1;
        }
        if (rate >= 2000) {
            point = 2;
        }
        if (rate >= 3000) {
            point = 3;
        }
        if (rate >= 4000) {
            point = 4;
        }
        if (rate >= 5000) {
            point = 5;
        }
        return point;
    }

    changeMenuQuiz = (e, {name}) => this.setState({activeItem: name});

    render() {
        const {params} = this.props;
        const {playID} = params;
        const {activeItem} = this.state;
        const dataQuiz = this.state.quizData;
        const img = dataQuiz.img;
        //const imgSmall = dataQuiz.img.small; console.log(img);
        return (
            <div>
                {!this.state.loaderStatus && <OnLoader/>}
                {this.state.loaderStatus && (
                    <div>
                        <Container className='play_container'>
                            <Header as="h1">
                                {dataQuiz.title}
                            </Header>
                            <div className='boxDetailQuiz'>
                                <p className='detailQuiz'>{dataQuiz.desc}</p>
                                <div className='ratingOfQuiz'>
                                    <Rating
                                        maxRating={5}
                                        defaultRating={this.calRate(dataQuiz.review_point)}
                                        icon='star'
                                        disabled/> {this.calRate(dataQuiz.review_point)}
                                    (<NumberFormat
                                        value={dataQuiz.review_point}
                                        displayType={'text'}
                                        thousandSeparator={true}/>
                                    &nbsp;คะแนน)
                                </div>
                                <ul className='quizByUser'>
                                    <li className='createQuizBy'><Icon name='user'/>สร้างโดย -
                                        <Link to='/'>
                                            {dataQuiz.create_by}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to='/'>การศึกษา</Link>
                                    </li>
                                    <li>
                                        <Link to='/'>ความรู้ทั่วไป</Link>
                                    </li>
                                </ul>
                            </div>
                            <Grid>
                                <Grid.Row>
                                    <Grid.Column width={10}>
                                        <Image src={img.small} width='100%'/>
                                    </Grid.Column>
                                    <Grid.Column width={6}>
                                        <p className='freeText'>{this.payConver(dataQuiz.quiz_pay)}</p>
                                        <p className='btn_play_p'>
                                            {!this.state.playEnable
                                                ? (
                                                    <Button onClick={this.playQuiz} positive>เล่นควิชนี้</Button>
                                                )
                                                : (
                                                    <Button>คุณกำลังควิชในขณะนี้..</Button>
                                                )}
                                        </p>
                                        <p className='detailHead'>รายละเอียดควิชย่อๆ</p>
                                        <ul className="list-detail-quiz">
                                            <li>
                                                <span className='flot-item-left'>จำนวนข้อสอบ</span>
                                                <span className='flot-item-right'>{dataQuiz.total_quest}
                                                    ข้อ</span>
                                            </li>
                                            <li>
                                                <span className='flot-item-left'>ทดสอบไปแล้ว</span>
                                                <span className='flot-item-right'>{dataQuiz.test_count}
                                                    ครั้ง</span>
                                            </li>
                                            <li>
                                                <span className='flot-item-left'>ภาษา</span>
                                                <span className='flot-item-right'>{this.langConvert(dataQuiz.quiz_language)}</span>
                                            </li>
                                            <li>
                                                <span className='flot-item-left'>คะแนนสูงสุด</span>
                                                <span className='flot-item-right'>{dataQuiz.top_player}</span>
                                            </li>
                                        </ul>
                                        <p className='addToForev'>
                                            {!this.state.addToWL
                                                ? (
                                                    <span onClick={this.addToWL}><Icon size='large' name='empty heart'/>
                                                        เพิ่มเป็นรายการโปรด</span>
                                                )
                                                : (
                                                    <span onClick={this.addToWL}><Icon size='large' color='red' name='heart'/>
                                                        ยกเลิกรายการโปรด</span>
                                                )}
                                        </p>
                                    </Grid.Column>
                                </Grid.Row>
                            </Grid>
                        </Container>
                        <div className='menuDetailQuiz'>
                            <Menu pointing secondary>
                                <Container className='play_container no-pan-mar'>
                                    <Menu.Item
                                        name='menu1'
                                        active={activeItem === 'menu1'}
                                        onClick={this.changeMenuQuiz}>
                                        <Link to={'/play/' + playID + '#detail'}>รายละเอียดควิช</Link>
                                    </Menu.Item>
                                    <Menu.Item
                                        name='menu2'
                                        active={activeItem === 'menu2'}
                                        onClick={this.changeMenuQuiz}>
                                        <Link to={'/play/' + playID + '#question'}>หัวข้อควิช</Link>
                                    </Menu.Item>
                                    <Menu.Item
                                        name='menu3'
                                        active={activeItem === 'menu3'}
                                        onClick={this.changeMenuQuiz}>
                                        <Link to={'/play/' + playID + '#ranking'}>อันดับผู้เล่น</Link>
                                    </Menu.Item>
                                    <Menu.Item
                                        name='menu4'
                                        active={activeItem === 'menu4'}
                                        onClick={this.changeMenuQuiz}>
                                        <Link to={'/play/' + playID + '#review'}>รีวิว/คอมเมนต์</Link>
                                    </Menu.Item>
                                </Container>
                            </Menu>
                        </div>
                        <div className='segDetailZone'>
                            <Container className='play_container no-pan-segDetailZone'>
                                <Grid>
                                    <Grid.Column width={10}>
                                        <div className='segDetail1'>
                                            <Header as="h1">
                                                รายละเอียดควิช
                                            </Header>
                                            <QuizDetail playID={playID}/>
                                        </div>
                                        <div className='segDetail2'>
                                            <Header as="h1">
                                                หัวข้อควิช
                                            </Header>
                                            <ListOnQuiz numShow='5' playID={playID}/>
                                        </div>
                                        <div className='segDetail3'>
                                            <Header as="h1">
                                                ทำเนียบผู้เล่น
                                            </Header>
                                            <ListPlayerTop numShow='5' playID={playID}/>
                                        </div>
                                        <div className='segDetail4'>
                                            <Header as="h1">
                                                รีวิว/คอมเมนต์
                                            </Header>
                                            <ListCommentQuiz numShow='6' playID={playID}/>
                                        </div>
                                    </Grid.Column>
                                    <Grid.Column width={6}></Grid.Column>
                                </Grid>
                            </Container>
                        </div>
                    </div>
                )}
            </div>
        );
    }
}

export default Play;