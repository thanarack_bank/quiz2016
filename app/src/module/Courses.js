import React, {Component} from 'react';
import axios from 'axios';
import ListQuiz from './ListQuiz';
//import {HlistQuiz} from '../http/HlistQuiz';
import OnLoader from './OnLoader';

class Courses extends Component {

    constructor(props) {
        super(props);
        this.state = {
            postQuiz: [],
            loaderStatus: false
        }
    }

    componentDidMount() {
        axios
            .get('http://localhost:3001/get/quiz')
            .then(res => {
                const postQuiz = res.data;
                this.setState((prevState) => ({
                    postQuiz: postQuiz,
                    loaderStatus: !prevState.loaderStatus
                }));

            });
    }

    componentWillUnmount() {
        this.setState({postQuiz: []});
    }

    render() {
        return (
            <div>
                {!this.state.loaderStatus && <OnLoader/>}
                {this.state.loaderStatus && <ListQuiz dataQuiz={this.state.postQuiz} perRow='6'/>}
            </div>
        );
    }
}

export default Courses;