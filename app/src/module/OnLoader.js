import React, {Component} from 'react';
import {Container, Loader} from 'semantic-ui-react'

class OnLoader extends Component {

    constructor(props) {
        super(props);
        /*this.state = {
            LoaderProcess: true
        }*/
    }

    componentDidMount() {
        //this.setState({LoaderProcess: false});
    }

    componentWillMount() {
        //this.setState({LoaderProcess: true});
    }

    render() {
        return (
            <div>
                <Container><Loader active inline='centered'/></Container>
            </div>
        );
    }

}

export default OnLoader;