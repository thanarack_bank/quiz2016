import React, {Component} from 'react';
import {Link} from 'react-router';
import {Menu, Input, Dropdown, Icon, Image} from 'semantic-ui-react';
import nprogress from 'nprogress';
import './Header.css';

class HeaderMain extends Component {

    state = {}

    componentWillMount() {
        nprogress.start();
    }

    componentDidMount() {
        nprogress.done();
    }
    //handleItemClick = (e, {name, tagLink}) => this.setState({activeItem: name})

    render() {
        //const {activeItem} = this.state;
        return (
            <div>
                <div className="border_header">
                    <a className="heder_boder_top"></a>
                </div>
                <div className="ui menu sticky">
                    <Menu.Item>
                        <Link to='/'><Icon name='smile' size='big'/></Link>
                    </Menu.Item>
                    <Menu.Item>
                        <Link to='/courses'>รวมควิช</Link>
                    </Menu.Item>
                    <Menu.Item as={Dropdown} text='หมวดหมู่'>
                        <Dropdown.Menu>
                            <Dropdown.Item>
                                <Link to="/categored/ข้อสอบไทย">ข้อสอบไทย</Link>
                            </Dropdown.Item>
                            <Dropdown.Item>
                                <Link to="/categored/สำหรับเด็ก">สำหรับเด็ก</Link>
                            </Dropdown.Item>
                            <Dropdown.Item>
                                <Link to="/categored/ทั่วไป">ทั่วไป</Link>
                            </Dropdown.Item>
                        </Dropdown.Menu>
                    </Menu.Item>
                    <Menu.Item>
                        <Input
                            action={{
                            type: 'submit',
                            content: 'ไป'
                        }}
                            placeholder='ค้นหาคลิชโดนๆ'/>
                    </Menu.Item>
                    <Menu.Menu position='right'>
                        <Menu.Item as={Dropdown} text='จัดการบัญชี'>
                            <Dropdown.Menu>
                                <Dropdown.Item>
                                    <Link to="/">สร้างควิชใหม่</Link>
                                </Dropdown.Item>
                                <Dropdown.Item>
                                    <Link to="/">ควิชของฉัน</Link>
                                </Dropdown.Item>
                                <Dropdown.Item>
                                    <Link to="/">บันทึกควิชที่ผ่านมา</Link>
                                </Dropdown.Item>
                                <Dropdown.Divider/>
                                <Dropdown.Item>
                                    <Link to="/">การตั้งค่า</Link>
                                </Dropdown.Item>
                                <Dropdown.Item>
                                    <Link to="/">ประวัติการชำระเงิน</Link>
                                </Dropdown.Item>
                                <Dropdown.Item>
                                    <Link to="/">ความช่วยเหลือ</Link>
                                </Dropdown.Item>
                                <Dropdown.Item>
                                    <Link to="/">ออกจากระบบ</Link>
                                </Dropdown.Item>
                            </Dropdown.Menu>
                        </Menu.Item>
                        <Menu.Item>
                            <Link to='/'>
                                <Image src='http://semantic-ui.com/images/avatar/small/elliot.jpg'width='32' />
                            </Link>
                        </Menu.Item>
                    </Menu.Menu>
                </div>
            </div>
        );
    }

}

export default HeaderMain;