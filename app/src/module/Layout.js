import React, {Component} from 'react';
//import {Link} from 'react-router';
import HeaderMain from './Header';

//Theme sematicUIFor React
import {Container} from 'semantic-ui-react';

class Layout extends Component {


    render() {
        const playComponent = this.props.routes[1].path;
        var classContainer = null;
        if (playComponent === 'play/:playID') {
            classContainer = 'no_con';
        }
        return (
            <div>
                <HeaderMain/>
                 {classContainer != 'no_con'
                    ? (
                        <Container>{this.props.children}</Container>
                    )
                    : (
                        <div>{this.props.children}</div>
                    )}
            </div>
        );
    }

}

export default Layout;